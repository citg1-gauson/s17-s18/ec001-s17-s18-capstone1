package com.zuitt.discussion.models;

import javax.persistence.*;

// Marks this java object as a representation of an entity/record from the database table "posts".
@Entity
//Designate the table name related to the model.
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Double price;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;
    //    Constructors
    public Course(){}
    public Course(String name,String description,Double price){
        this.name = name;
        this.description = description;
        this.price = price;
    }

    //getters and setters
    public  Long getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getDescription(){
        return description;
    }
    public Double getPrice(){
        return price;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setPrice(Double price){
        this.price = price;
    }
    public User getUser(){
        return user;
    }
    public void setUser(User user){
        this.user = user;
    }
}

