package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String username;
    @Column
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @OneToMany(mappedBy = "user")
//    Prevent infinite recursion with bidirectional relationship.
    @JsonIgnore
//    "Set" class is a collection that contains no duplicate elements.
    private Set<Course> courses;
    //constructor
    public User(){}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    // Getters and Setters
    public  Long getId(){
        return id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public  Set<Course> getCourses(){
        return courses;
    }
    public void setCourses(Set<Course> courses){
        this.courses = courses;
    }
}
