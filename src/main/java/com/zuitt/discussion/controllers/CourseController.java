package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//Enables cross origin request via @CrossOrigin.
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

    //    Create course
    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.createPost(stringToken,course);
        return new ResponseEntity<>("Course created successfully.", HttpStatus.CREATED);
    }
    // Delete a course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable Long courseid, @RequestHeader(value = "Authorization") String stringToken) {
        return courseService.deleteCourse(courseid,stringToken);
    }
    // Update a course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedPost(@PathVariable Long courseid, @RequestBody Course course, @RequestHeader(value = "Authorization") String stringToken) {
        return courseService.updateCourse(courseid,stringToken, course);
    }
    //    Get all course
    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost() {
        return new ResponseEntity<>(courseService.getCourse(), HttpStatus.OK);
    }
}

