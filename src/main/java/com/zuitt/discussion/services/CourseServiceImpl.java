package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
//Will allow us to use the CRUD methods inherited from the CRUDRepository
@Service
public class CourseServiceImpl implements CourseService {

    //    An object cannot be instantiated from interfaces.
    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
     JwtToken jwtToken;

    //    Create course
    public void createPost(String stringToken,Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    //Get All Course
    public Iterable<Course> getCourse(){
        return courseRepository.findAll();
    }
    //delete
    public ResponseEntity deleteCourse(Long id,String stringToken) {
        Course courseForDeleting = courseRepository.findById(id).get();
        String courseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }
    }
    //update
    public ResponseEntity updateCourse(Long id,String stringToken, Course course) {
        Course courseForUpdating = courseRepository.findById(id).get();
        String courseAuthorName = courseForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());
            courseRepository.save(courseForUpdating);
            return  new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this Course.", HttpStatus.UNAUTHORIZED);
        }

    }

}
