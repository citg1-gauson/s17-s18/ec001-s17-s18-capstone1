// Service is an interface that exposes the methods of an implementation whose details have been abstracted away.
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    //create course
    void createPost(String stringToken,Course course);
    //Viewing all course
    Iterable<Course> getCourse();
    //delete course
    ResponseEntity deleteCourse(Long id,String stringToken);
    //update course
    ResponseEntity updateCourse(Long id,String stringToken, Course course);
}
